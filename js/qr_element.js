(function ($) {
    Drupal.behaviors.qrElement = {

        attach: function (context, settings) {

            $('.qr-element-div', context).each(function () {

              var text = ""+$(this).data('value');
              var width = $(this).data('width');
              var height = $(this).data('height');

              if (width && height) {
                $(this).qrcode({
                  text: text,
                  width: width,
                  height: height
                });
              }
              else {
                $(this).qrcode(text);
              }
            });

        }

    };
})(jQuery);
